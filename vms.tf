##### VMs Part #####

resource "yandex_compute_instance" "monitor" {
  name        = "monitor"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "2"
    memory        = "2"
  }
  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      size = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

