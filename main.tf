##### Variables #####

locals {
  local_data = jsondecode(file("${path.module}/config.json"))
}

##### General #####

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "vault" {
  address = "http://127.0.0.1:8200"
  skip_tls_verify = true
}

data "vault_generic_secret" "yc" {
  path = "secret/yc"
}

provider "yandex" {
  token                    = data.vault_generic_secret.yc.data["YC_TOKEN"]
  cloud_id                 = local.local_data.CLOUD_ID
  folder_id                = local.local_data.FOLDER_ID
  zone                     = local.local_data.ZONE
}


##### Net & Subnet definition #####

resource "yandex_vpc_network" "network-1" {
  name = "network-1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet-1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.1.0/24"]
}
