### 1. Инструкции 

https://cloud.yandex.ru/marketplace/products/yc/vault-yckms

https://cloud.yandex.ru/docs/kms/tutorials/vault-secret

https://cloud.yandex.ru/docs/managed-kubernetes/operations/applications/hashicorp-vault

### 2. В соответствии с инструкциями создаем в Яндекс Облаке ВМ с Vault или запускаем контейнер в соответствии с п.5

### 3. Создаем временный сервисный аккаунт с правами на шифрование/дешифрование:

yc iam service-account create --name vault-kms

yc iam key create --service-account-name vault-kms --output authorized-key.json

yc kms symmetric-key create --name example-key --default-algorithm aes-256 --rotation-period 24h

yc resource-manager folder add-access-binding --id <*идентификатор каталога*> --service-account-name vault-kms --role kms.keys.encrypterDecrypter

### 4. Конфиг vault 

```
ui = true
storage "file" {
  path = "/opt/vault"
}
seal "yandexcloudkms" {
  kms_key_id               = "abjurgl8esea0iir0g51"
  service_account_key_file = "/vault/config/key.json"
}
```

### 5. Синтаксис запуска docker

docker run --cap-add=IPC_LOCK -d --name=dev-vault -e 'VAULT_DEV_ROOT_TOKEN_ID=myroot' -p 8200:8200 --name my_vault cr.yandex/yc/vault

docker run --cap-add=IPC_LOCK --volume /home/petr/vault-test/config/:/vault/config/ -e 'VAULT_DEV_ROOT_TOKEN_ID=myroot' -d -p 8200:8200 --name my_vault cr.yandex/yc/vault

### 6. В веб-морде добавляем kv, в нашем случае это YC_TOKEN

### 7. Передаем в переменную окружения VAULT_TOKEN:

export VAULT_TOKEN=<*vault_token*>

### 8. Конфиг терраформ

```
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "vault" {
  address = "http://127.0.0.1:8200"
  skip_tls_verify = true
}

data "vault_generic_secret" "yc" {
  path = "kv/yc"
}

provider "yandex" {
  token                    = data.vault_generic_secret.yc.data["YC_TOKEN"]
  cloud_id                 = local.local_data.CLOUD_ID
  folder_id                = local.local_data.FOLDER_ID
  zone                     = local.local_data.ZONE
}
```